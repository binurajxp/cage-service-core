package com.enginef.cage.domain.model.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.enginef.cage.domain.model.entity.id.EventDataId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import org.springframework.data.annotation.Id;

@DynamoDBTable(tableName = "EventData")
public class EventDataEntity implements Serializable {


  @JsonIgnore
  @DynamoDBIgnore
  @Id
  private EventDataId eventDataId;

  @DynamoDBHashKey(attributeName = "EventDataKey")
  public String getEventDataKey() {
    return eventDataId != null ? eventDataId.getEventDataKey() : null;
  }

  public void setEventDataKey(String eventDataKey) {
    if (eventDataId == null) {
      eventDataId = new EventDataId();
    }
    eventDataId.setEventDataKey(eventDataKey);
  }

  @DynamoDBRangeKey(attributeName = "EventTimeStamp")
  public Double getEventTimeStamp() {
    return eventDataId != null ? eventDataId.getEventTimeStamp() : null;
  }

  public void setEventTimeStamp(Double eventTimeStamp) {
    if (eventDataId == null) {
      eventDataId = new EventDataId();
    }
    eventDataId.setEventTimeStamp(eventTimeStamp);
  }

}

