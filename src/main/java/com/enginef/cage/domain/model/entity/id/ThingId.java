package com.enginef.cage.domain.model.entity.id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import java.io.Serializable;

public final class ThingId implements Serializable {

  @DynamoDBHashKey(attributeName = "actorId")
  private String actorId;
  @DynamoDBRangeKey(attributeName = "id")
  private String id;

  public String getActorId() {
    return actorId;
  }

  public ThingId withActorId(String actorId) {
    this.actorId = actorId;
    return this;
  }

  public String getId() {
    return id;
  }

  public ThingId withId(String id) {
    this.id = id;
    return this;
  }

  public void setActorId(String actorId) {
    this.actorId = actorId;
  }

  public void setId(String id) {
    this.id = id;
  }
}
