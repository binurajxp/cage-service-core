package com.enginef.cage.domain.model.entity.actor;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.enginef.cage.domain.model.converter.DescriptorConverter;
import com.enginef.cage.domain.model.converter.ListDataConverter;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@DynamoDBTable(tableName = "project-cage-apps-dev-actor")
public class ActorEntity implements Serializable {

  @DynamoDBHashKey(attributeName = "id")
  public String id;

  @DynamoDBAttribute(attributeName = "createdBy")
  public String createdBy;

  @DynamoDBAttribute(attributeName = "createdOn")
  public Long createdOn;

//  @DynamoDBTypeConverted(converter = DescriptorConverter.class)
//  @DynamoDBAttribute(attributeName = "descriptors")
//  public Map<String, Object> descriptors;

  @DynamoDBAttribute(attributeName = "isActive")
  public Boolean isActive;

  @DynamoDBAttribute(attributeName = "modifiedBy")
  public String modifiedBy;

  @DynamoDBAttribute(attributeName = "modifiedOn")
  public Long modifiedOn;

  @DynamoDBAttribute(attributeName = "name")
  public String name;

  @DynamoDBAttribute(attributeName = "ref")
  public String ref;

  @DynamoDBAttribute(attributeName = "referrer")
  public String referrer;

  @DynamoDBAttribute(attributeName = "type")
  public String type;

//  @DynamoDBTypeConverted(converter = ListDataConverter.class)
//  //@DynamoDBTyped(value = L)
//  @DynamoDBAttribute(attributeName = "address")
//  public Object address;
//
//  @DynamoDBTypeConverted(converter = ListDataConverter.class)
//  //@DynamoDBTyped(value = L)
//  @DynamoDBAttribute(attributeName = "contactNumbers")
//  public Object contactNumbers;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

//  public Object getAddress() {
//    return address;
//  }
//
//  public void setAddress(Object address) {
//    this.address = address;
//  }
//
//  public Object getContactNumbers() {
//    return contactNumbers;
//  }
//
//  public void setContactNumbers(Object contactNumbers) {
//    this.contactNumbers = contactNumbers;
//  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Long createdOn) {
    this.createdOn = createdOn;
  }

//  public Map<String, Object> getDescriptors() {
//    return descriptors;
//  }
//
//  public void setDescriptors(Map<String, Object> descriptors) {
//    this.descriptors = descriptors;
//  }

  public Boolean getActive() {
    return isActive;
  }

  public void setActive(Boolean active) {
    isActive = active;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public Long getModifiedOn() {
    return modifiedOn;
  }

  public void setModifiedOn(Long modifiedOn) {
    this.modifiedOn = modifiedOn;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRef() {
    return ref;
  }

  public void setRef(String ref) {
    this.ref = ref;
  }

  public String getReferrer() {
    return referrer;
  }

  public void setReferrer(String referrer) {
    this.referrer = referrer;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
