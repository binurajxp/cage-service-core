package com.enginef.cage.domain.model.converter;

import static com.amazonaws.services.dynamodbv2.document.ItemUtils.toAttributeValue;
import static com.amazonaws.services.dynamodbv2.document.ItemUtils.toSimpleValue;
import static java.util.stream.Collectors.toMap;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import java.util.Map;
import java.util.Map.Entry;

public class DescriptorConverter implements DynamoDBTypeConverter<Map<String, AttributeValue>, Map<String, Object>> {

  @Override
  public Map<String, AttributeValue> convert(Map<String, Object> object) {
    return object != null ? object.entrySet().stream().collect(
        toMap(Entry::getKey, item -> toAttributeValue(item.getValue()))) : null;
  }

  @Override
  public Map<String, Object> unconvert(Map<String, AttributeValue> object) {

    return object != null ? object.entrySet().stream().collect(
        toMap(Entry::getKey, item -> toSimpleValue(item.getValue()))) : null;
  }
}
