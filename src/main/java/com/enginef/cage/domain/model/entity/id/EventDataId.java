package com.enginef.cage.domain.model.entity.id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import java.io.Serializable;

public final class EventDataId implements Serializable {

  @DynamoDBHashKey(attributeName = "EventDataKey ")
  private String eventDataKey;
  @DynamoDBRangeKey(attributeName = "EventTimeStamp")
  private Double eventTimeStamp;

  public String getEventDataKey() {
    return eventDataKey;
  }

  public void setEventDataKey(String eventDataKey) {
    this.eventDataKey = eventDataKey;
  }

  public Double getEventTimeStamp() {
    return eventTimeStamp;
  }

  public void setEventTimeStamp(Double eventTimeStamp) {
    this.eventTimeStamp = eventTimeStamp;
  }
}
