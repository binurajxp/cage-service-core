package com.enginef.cage.domain.model.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.enginef.cage.domain.model.converter.DescriptorConverter;
import java.io.Serializable;
import java.util.Map;

@DynamoDBTable(tableName = "project-cage-apps-dev-thing")
public class ThingIndexEntity implements Serializable {

  private static final String GSI_INDEX_THING_BY_STRUCTURE_KEY = "structureKey-id-index";

  @DynamoDBHashKey(attributeName = "actorId")
  private String actorId;

  @DynamoDBIndexRangeKey(attributeName = "id",
      globalSecondaryIndexName = GSI_INDEX_THING_BY_STRUCTURE_KEY)
  private String id;

  @DynamoDBAttribute(attributeName = "createdBy")
  private String createdBy;

  @DynamoDBAttribute(attributeName = "createdOn")
  private Double createdOn;

  @DynamoDBTypeConverted(converter = DescriptorConverter.class)
  @DynamoDBAttribute(attributeName = "descriptors")
  private Map<String, Object> descriptors;

  @DynamoDBAttribute(attributeName = "isActive")
  private Boolean isActive;

  @DynamoDBAttribute(attributeName = "modifiedBy")
  private String modifiedBy;

  @DynamoDBAttribute(attributeName = "modifiedOn")
  private Double modifiedOn;

  @DynamoDBAttribute(attributeName = "name")
  private String name;

  @DynamoDBIndexHashKey(attributeName = "structureKey",
      globalSecondaryIndexName = GSI_INDEX_THING_BY_STRUCTURE_KEY)
  private String structureKey;

  @DynamoDBAttribute(attributeName = "structureVersion")
  private String structureVersion;

  @DynamoDBAttribute(attributeName = "thingType")
  private String thingType;

  @DynamoDBAttribute(attributeName = "validFrom")
  private Double validFrom;

  @DynamoDBAttribute(attributeName = "validTill")
  private Double validTill;

  public String getActorId() {
    return actorId;
  }

  public ThingIndexEntity setActorId(String actorId) {
    this.actorId = actorId;
    return this;
  }

  public String getId() {
    return id;
  }

  public ThingIndexEntity setId(String id) {
    this.id = id;
    return this;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public ThingIndexEntity setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public Double getCreatedOn() {
    return createdOn;
  }

  public ThingIndexEntity setCreatedOn(Double createdOn) {
    this.createdOn = createdOn;
    return this;
  }

  public Map<String, Object> getDescriptors() {
    return descriptors;
  }

  public ThingIndexEntity setDescriptors(Map<String, Object> descriptors) {
    this.descriptors = descriptors;
    return this;
  }

  public Boolean getActive() {
    return isActive;
  }

  public ThingIndexEntity setActive(Boolean active) {
    isActive = active;
    return this;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public ThingIndexEntity setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  public Double getModifiedOn() {
    return modifiedOn;
  }

  public ThingIndexEntity setModifiedOn(Double modifiedOn) {
    this.modifiedOn = modifiedOn;
    return this;
  }

  public String getName() {
    return name;
  }

  public ThingIndexEntity setName(String name) {
    this.name = name;
    return this;
  }

  public String getStructureKey() {
    return structureKey;
  }

  public ThingIndexEntity setStructureKey(String structureKey) {
    this.structureKey = structureKey;
    return this;
  }

  public String getStructureVersion() {
    return structureVersion;
  }

  public ThingIndexEntity setStructureVersion(String structureVersion) {
    this.structureVersion = structureVersion;
    return this;
  }

  public String getThingType() {
    return thingType;
  }

  public ThingIndexEntity setThingType(String thingType) {
    this.thingType = thingType;
    return this;
  }

  public Double getValidFrom() {
    return validFrom;
  }

  public ThingIndexEntity setValidFrom(Double validFrom) {
    this.validFrom = validFrom;
    return this;
  }

  public Double getValidTill() {
    return validTill;
  }

  public ThingIndexEntity setValidTill(Double validTill) {
    this.validTill = validTill;
    return this;
  }
}

