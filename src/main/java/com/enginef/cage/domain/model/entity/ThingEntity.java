package com.enginef.cage.domain.model.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperFieldModel.DynamoDBAttributeType;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTyped;
import com.enginef.cage.domain.model.converter.DescriptorConverter;
import com.enginef.cage.domain.model.entity.id.ThingId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Map;
import org.springframework.data.annotation.Id;

@DynamoDBTable(tableName = "project-cage-apps-dev-thing")
public class ThingEntity implements Serializable {


  @JsonIgnore
  @DynamoDBIgnore
  @Id
  private ThingId thingId;

  @DynamoDBAttribute(attributeName = "createdBy")
  private String createdBy;

  @DynamoDBAttribute(attributeName = "createdOn")
  private Double createdOn;

  @DynamoDBTypeConverted(converter = DescriptorConverter.class)
  @DynamoDBAttribute(attributeName = "descriptors")
  private Map<String, Object> descriptors;

  @JsonProperty(value = "isActive")
  @DynamoDBTyped(DynamoDBAttributeType.BOOL)
  @DynamoDBAttribute(attributeName = "isActive")
  private Boolean active;

  @DynamoDBAttribute(attributeName = "modifiedBy")
  private String modifiedBy;

  @DynamoDBAttribute(attributeName = "modifiedOn")
  private Double modifiedOn;

  @DynamoDBAttribute(attributeName = "name")
  private String name;

  @DynamoDBAttribute(attributeName = "structureKey")
  private String structureKey;

  @DynamoDBAttribute(attributeName = "structureVersion")
  private String structureVersion;

  @DynamoDBAttribute(attributeName = "thingType")
  private String thingType;

  @DynamoDBAttribute(attributeName = "validFrom")
  private Double validFrom;

  @DynamoDBAttribute(attributeName = "validTill")
  private Double validTill;

  @DynamoDBHashKey(attributeName = "actorId")
  public String getActorId() {
    return thingId != null ? thingId.getActorId() : null;
  }

  public void setActorId(String actorId) {
    if (thingId == null) {
      thingId = new ThingId();
    }
    thingId.setActorId(actorId);
  }

  @DynamoDBRangeKey(attributeName = "id")
  public String getId() {
    return thingId != null ? thingId.getId() : null;
  }

  public void setId(String id) {
    if (thingId == null) {
      thingId = new ThingId();
    }
    thingId.setId(id);
  }

  public ThingEntity(ThingId thingId) {
    this.thingId = thingId;
  }

  public ThingEntity() {
  }

  public ThingId getThingId() {
    return thingId;
  }

  public void setThingId(ThingId thingId) {
    this.thingId = thingId;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Double getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Double createdOn) {
    this.createdOn = createdOn;
  }

  public Map<String, Object> getDescriptors() {
    return descriptors;
  }

  public void setDescriptors(
      Map<String, Object> descriptors) {
    this.descriptors = descriptors;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public Double getModifiedOn() {
    return modifiedOn;
  }

  public void setModifiedOn(Double modifiedOn) {
    this.modifiedOn = modifiedOn;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getStructureKey() {
    return structureKey;
  }

  public void setStructureKey(String structureKey) {
    this.structureKey = structureKey;
  }

  public String getStructureVersion() {
    return structureVersion;
  }

  public void setStructureVersion(String structureVersion) {
    this.structureVersion = structureVersion;
  }

  public String getThingType() {
    return thingType;
  }

  public void setThingType(String thingType) {
    this.thingType = thingType;
  }

  public Double getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Double validFrom) {
    this.validFrom = validFrom;
  }

  public Double getValidTill() {
    return validTill;
  }

  public void setValidTill(Double validTill) {
    this.validTill = validTill;
  }
}

