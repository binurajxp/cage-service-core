package com.enginef.cage.domain.model.converter;

import static com.amazonaws.services.dynamodbv2.document.ItemUtils.toSimpleList;
import static org.springframework.util.CollectionUtils.isEmpty;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import java.util.List;

public class ListDataConverter implements DynamoDBTypeConverter<List<AttributeValue>, Object> {

  @Override
  public List<AttributeValue> convert(Object object) {
    return null;
  }

  @Override
  public Object unconvert(List<AttributeValue> object) {
    return !isEmpty(object) ? toSimpleList(object) : null;
  }
}
