package com.enginef.cage.domain.service;

import com.enginef.cage.domain.model.entity.ThingEntity;
import com.enginef.cage.domain.model.entity.actor.ActorEntity;
import java.util.List;
import java.util.UUID;

public interface ThingService {

  List<ThingEntity> getByActorIdAndStructureKeys(UUID actorId, List<String> structureKeys);

  List<ThingEntity> delete(List<String> structureKeys);

  List<ActorEntity> getActorsByName(String name);

  List<ThingEntity> delete(String structureKey, String filter, String filterValue);

  void deleteEventData(String eventDataName);

  List<ThingEntity> getThings(String structureKey, String filter, String filterValue);

  List<ThingEntity> getThingsByActorIds(List<UUID> actorIds, String structureKey, String filter, String filterValue);

  List<UUID> getActorsWithNoGoalsPublished(List<UUID> actorIds);

  List<ThingEntity> updateThingsByActorIds(List<UUID> actorIds, String structureKey, String filter, String filterValue, String updateKey, String updateValue);

  List<ThingEntity> updateThingsByIds(String actorId, List<String> ids, String updateKey, String updateValue);
}
