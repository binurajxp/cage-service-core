package com.enginef.cage.domain.service.impl;

import static java.util.UUID.fromString;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.CollectionUtils.isEmpty;

import com.enginef.cage.domain.model.entity.EventDataEntity;
import com.enginef.cage.domain.model.entity.ThingEntity;
import com.enginef.cage.domain.model.entity.actor.ActorEntity;
import com.enginef.cage.domain.repository.ThingCustomRepository;
import com.enginef.cage.domain.repository.ThingRepository;
import com.enginef.cage.domain.repository.actor.ActorRepository;
import com.enginef.cage.domain.service.ThingService;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class ThingServiceImpl implements ThingService {

  private static final String X_WRIST_PUBLISHED_DATAS = "x-wrist-published-datas";
  private static final String DESCRIPTORS_PUBLISHED_TYPE = "descriptors.publishedType";
  private static final String GOAL = "Goal";
  private ThingRepository thingRepository;

  private ThingCustomRepository thingCustomRepository;

  private ActorRepository actorRepository;

  @Autowired
  public ThingServiceImpl(ThingRepository thingRepository,
      ThingCustomRepository thingCustomRepository,
      ActorRepository actorRepository) {
    this.thingRepository = thingRepository;
    this.thingCustomRepository = thingCustomRepository;
    this.actorRepository = actorRepository;
  }

  @Override
  public List<ThingEntity> getByActorIdAndStructureKeys(UUID actorId, List<String> structureKeys) {
    List<ThingEntity> thingEntities = thingRepository.findByActorIdAndStructureKeyIn(actorId.toString(), structureKeys);
    thingCustomRepository.deleteThingData(thingEntities);
    return thingEntities;
  }

  @Override
  public List<ThingEntity> delete(List<String> structureKeys) {
    List<ThingEntity> thingEntities = thingRepository.findByStructureKeyIn(structureKeys);
    thingCustomRepository.deleteThingData(thingEntities);
    return thingEntities;
  }

  @Override
  public List<ActorEntity> getActorsByName(String name) {
    List<ActorEntity> actorEntities = actorRepository.findByName(name);
    thingCustomRepository.deleteAllActors(actorEntities);
    return actorEntities;
  }

  @Override
  public List<ThingEntity> delete(String structureKey, String filter, String filterValue) {
    List<ThingEntity> thingEntities = thingCustomRepository
        .findAllByStructureKeyWithFilter(structureKey, filter, filterValue);
    thingCustomRepository.deleteThingData(thingEntities);
    return thingEntities;
  }

  @Override
  public void deleteEventData(String eventDataName) {
    List<EventDataEntity> eventDataEntities = thingCustomRepository
        .findAllEventData(eventDataName);
    thingCustomRepository.deleteEventData(eventDataEntities);
  }

  @Override
  public List<ThingEntity> getThings(String structureKey, String filter, String filterValue) {
    return thingCustomRepository
        .findAllByStructureKeyWithFilter(structureKey, filter, filterValue);
  }

  @Override
  public List<ThingEntity> getThingsByActorIds(List<UUID> actorIds, String structureKey, String filter,
      String filterValue) {
    return thingCustomRepository
        .findAllByStructureKeyWithFilter(actorIds, structureKey, filter, filterValue);
  }

  @Override
  public List<UUID> getActorsWithNoGoalsPublished(List<UUID> actorIds) {
    List<ThingEntity> thingEntities = getThingsByActorIds(actorIds, X_WRIST_PUBLISHED_DATAS, DESCRIPTORS_PUBLISHED_TYPE,
        GOAL);
    if (thingEntities.isEmpty()) {
      return actorIds;
    }
    return thingEntities
        .stream()
        .map(thingEntity -> fromString(thingEntity.getActorId()))
        .filter(uuid -> !actorIds.contains(uuid))
        .collect(toList());
  }

  @Override
  public List<ThingEntity> updateThingsByActorIds(List<UUID> actorIds, String structureKey, String filter,
      String filterValue, String updateKey, String updateValue) {

    List<ThingEntity> thingEntities = getThingsByActorIds(actorIds, structureKey, filter,
        filterValue);
    thingEntities.forEach(thingEntity -> thingEntity.getDescriptors().put(updateKey, updateValue));
    thingCustomRepository.batchUpdate(thingEntities);
    return thingEntities;
  }

  @Override
  public List<ThingEntity> updateThingsByIds(String actorId, List<String> ids, String updateKey,
      String updateValue) {
    List<ThingEntity> thingEntities = thingCustomRepository.findAllByIds(actorId, ids);
    if (!isEmpty(thingEntities)) {
      thingEntities.forEach(thingEntity -> thingEntity.getDescriptors().put(updateKey, updateValue));
      thingCustomRepository.batchUpdate(thingEntities);
    }
    return thingEntities;
  }
}
