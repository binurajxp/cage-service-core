package com.enginef.cage.domain.repository;

import com.enginef.cage.domain.model.entity.ThingEntity;
import com.enginef.cage.domain.model.entity.id.ThingId;
import java.util.List;
import org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.stereotype.Repository;

@Repository
@EnableScan
public interface ThingRepository extends DynamoDBCrudRepository<ThingEntity, ThingId> {

  List<ThingEntity> findByActorId(String actorId);

  List<ThingEntity> findByActorIdAndStructureKeyIn(String actorId, List<String> structureKeys);

  List<ThingEntity> findByStructureKey(String structureKey);

  List<ThingEntity> findByStructureKeyIn(List<String> structureKeys);

  List<ThingEntity> findByActorIdAndIdIn(String actorId, List<String> ids);
}
