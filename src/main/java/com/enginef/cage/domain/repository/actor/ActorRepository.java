package com.enginef.cage.domain.repository.actor;

import com.enginef.cage.domain.model.entity.actor.ActorEntity;
import java.util.List;
import org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.stereotype.Repository;

@EnableScan
@Repository
public interface ActorRepository extends DynamoDBCrudRepository<ActorEntity, String> {

  List<ActorEntity> findByName(String name);
}
