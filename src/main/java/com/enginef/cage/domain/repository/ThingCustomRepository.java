package com.enginef.cage.domain.repository;

import static com.amazonaws.services.dynamodbv2.model.ComparisonOperator.IN;
import static java.lang.String.join;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.springframework.util.CollectionUtils.isEmpty;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.enginef.cage.domain.model.entity.EventDataEntity;
import com.enginef.cage.domain.model.entity.ThingEntity;
import com.enginef.cage.domain.model.entity.actor.ActorEntity;
import com.enginef.cage.domain.model.entity.id.ThingId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ThingCustomRepository {

  private static final String STRUCTURE_KEY = "structureKey";
  private static final String ID = "id";
  private static final String STRUCTURE_KEY_ID_INDEX = "structureKey-id-index";
  private static final String ACTOR_ID_STRUCTURE_KEY_INDEX = "actorId-structureKey-index";
  private DynamoDBMapper dynamoDBMapper;

  @Autowired
  public ThingCustomRepository(DynamoDBMapper dynamoDBMapper) {
    this.dynamoDBMapper = dynamoDBMapper;
  }


  public List<ThingEntity> findAllByStructureKey(String actorId, List<String> structureKeys) {
    List<AttributeValue> attributeValues = isEmpty(structureKeys) ? emptyList()
        : structureKeys.stream().map(item -> new AttributeValue().withS(item)).collect(toList());
    Map<String, Condition> eav = new HashMap<>();
    eav.put(STRUCTURE_KEY, new Condition().withComparisonOperator(IN).withAttributeValueList(attributeValues));
    DynamoDBQueryExpression<ThingEntity> dynamoDBQueryExpression = new DynamoDBQueryExpression<ThingEntity>()
        .withHashKeyValues(new ThingEntity(new ThingId().withActorId(actorId)))
        .withConsistentRead(true)
        .withQueryFilter(eav);
    return dynamoDBMapper.query(ThingEntity.class, dynamoDBQueryExpression);
  }

  public List<ThingEntity> findAllByIds(String actorId, List<String> ids) {
    Map<String, AttributeValue> eav = new HashMap<>();
    eav.put(":actorId", new AttributeValue().withS(actorId));
    DynamoDBQueryExpression<ThingEntity> dynamoDBQueryExpression = new DynamoDBQueryExpression<ThingEntity>()
        .withIndexName(ACTOR_ID_STRUCTURE_KEY_INDEX)
        .withKeyConditionExpression("actorId = :actorId")
        .withConsistentRead(false)
        .withExpressionAttributeValues(eav)
        .withFilterExpression("id IN (" + buildInFilterExpression(ids, eav) + ")");
    return dynamoDBMapper.query(ThingEntity.class, dynamoDBQueryExpression);
  }

  public List<ThingEntity> findAllByStructureKeyWithFilter(String structureKey, String filter, String filterValue) {
    String filterExpression = stream(filter.split("\\.")).map(item -> "#" + item).collect(Collectors.joining("."));
    Map<String, String> filterExpressionMap = stream(filter.split("\\."))
        .collect(toMap(item -> "#" + item, item -> item));
    Map<String, AttributeValue> eav = new HashMap<>();
    eav.put(":filterValue", new AttributeValue().withS(filterValue));
    eav.put(":structureKey", new AttributeValue().withS(structureKey));
    DynamoDBQueryExpression<ThingEntity> dynamoDBQueryExpression = new DynamoDBQueryExpression<ThingEntity>()
        .withIndexName(STRUCTURE_KEY_ID_INDEX)
        .withKeyConditionExpression("structureKey = :structureKey")
        .withFilterExpression(filterExpression + "=:filterValue")
        .withExpressionAttributeNames(filterExpressionMap)
        .withExpressionAttributeValues(eav)
        .withConsistentRead(false);

    return dynamoDBMapper.query(ThingEntity.class, dynamoDBQueryExpression);
  }

  public List<ThingEntity> findAllByStructureKeyWithFilter(List<UUID> actorIds, String structureKey, String filter,
      String filterValue) {

    AtomicInteger atomicInteger = new AtomicInteger(0);
    Map<String, String> actors = actorIds.stream()
        .collect(toMap(o -> ":actorId" + atomicInteger.getAndIncrement(), UUID::toString));
    String actorKeys = join(",", actors.keySet());
    Map<String, AttributeValue> actorFilterMap = new HashMap<>();
    actors.forEach((key, value) -> actorFilterMap.put(key, new AttributeValue().withS(value)));

    String filterExpression = stream(filter.split("\\.")).map(item -> "#" + item).collect(Collectors.joining("."));
    Map<String, String> filterExpressionMap = stream(filter.split("\\."))
        .collect(toMap(item -> "#" + item, item -> item));
    Map<String, AttributeValue> eav = new HashMap<>();
    eav.put(":filterValue", new AttributeValue().withS(filterValue));
    eav.put(":structureKey", new AttributeValue().withS(structureKey));
    eav.putAll(actorFilterMap);
    DynamoDBQueryExpression<ThingEntity> dynamoDBQueryExpression = new DynamoDBQueryExpression<ThingEntity>()
        .withIndexName(STRUCTURE_KEY_ID_INDEX)
        .withKeyConditionExpression("structureKey = :structureKey")
        .withFilterExpression(filterExpression + "=:filterValue and actorId in (" + actorKeys + ")")
        .withExpressionAttributeNames(filterExpressionMap)
        .withExpressionAttributeValues(eav)
        .withConsistentRead(false);

    return dynamoDBMapper.query(ThingEntity.class, dynamoDBQueryExpression);
  }

  public List<EventDataEntity> findAllEventData(String structureKey) {
    Map<String, AttributeValue> eav = new HashMap<>();
    eav.put(":workflowName", new AttributeValue().withS(structureKey));
    DynamoDBQueryExpression<EventDataEntity> dynamoDBQueryExpression = new DynamoDBQueryExpression<EventDataEntity>()
        .withIndexName("WorkflowName-index")
        .withKeyConditionExpression("WorkflowName = :workflowName")
        .withExpressionAttributeValues(eav)
        .withConsistentRead(false);

    return dynamoDBMapper.query(EventDataEntity.class, dynamoDBQueryExpression);
  }

  public void deleteAllActors(List<ActorEntity> actorEntities) {
    dynamoDBMapper.batchDelete(actorEntities);
  }

  public void deleteThingData(List<ThingEntity> thingEntities) {
    dynamoDBMapper.batchDelete(thingEntities);
  }

  public void deleteEventData(List<EventDataEntity> eventDataEntities) {
    dynamoDBMapper.batchDelete(eventDataEntities);
  }

  public void batchUpdate(List<ThingEntity> thingEntities){
    dynamoDBMapper.batchSave(thingEntities);
  }

  private String buildInFilterExpression(List<String> ids, Map<String, AttributeValue> valueMap) {
    StringBuilder builder = new StringBuilder();
    int i = 1;
    for (String id : ids) {
      builder.append(":id").append(i).append(",");
      valueMap.put(":id" + i, new AttributeValue().withS(id));
      i++;
    }
    return builder.toString().substring(0, builder.length() - 1);
  }
}


