package com.enginef.cage.domain.config;

import static com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder.standard;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import org.socialsignin.spring.data.dynamodb.core.DynamoDBOperations;
import org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDynamoDBRepositories(basePackages = "com.enginef.cage.domain.repository")
public class DynamoDBConfig {

  @Value("${amazon.dynamodb.endpoint}")
  private String amazonDynamoDBEndpoint;

  @Bean
  public AmazonDynamoDB amazonDynamoDB() {
    return standard().withEndpointConfiguration(endpointConfiguration())
        .build();
  }

  @Bean
  public EndpointConfiguration endpointConfiguration() {
    return new EndpointConfiguration(amazonDynamoDBEndpoint, null);
  }

  @Bean
  public DynamoDBOperations dynamoDBOperations() {
    return new DynamoDBTemplate(amazonDynamoDB());
  }

  @Bean
  public DynamoDBMapper dynamoDBMapper() {
    return new DynamoDBMapper(amazonDynamoDB());
  }

  @Bean
  public DynamoDB dynamoDB() {
    return new DynamoDB(amazonDynamoDB());
  }
}
