package com.enginef.cage.domain.dao;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.enginef.cage.domain.utils.CommonUtils;
import com.enginef.cage.domain.utils.JsonPathReader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

@Component
public class DynamicDBReader {
    String projectionExpression = "";
    @Autowired
    DynamoDB dynamoDB;

    public String readFromDatabase(String queryExpression
            , String toJSONSchema
    ) {

        projectionExpression = "";
        Map<String, Object> projectionMap = new HashMap<String, Object>();
        Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
        String returnString = "[";
        String record = "";
        JSONObject readerJSON = new JSONObject(queryExpression);
        String tableName = readerJSON.getString("Table");
        Table table = dynamoDB.getTable(tableName);

        QuerySpec spec = new QuerySpec();
        ScanSpec scspec = new ScanSpec();

        if (!readerJSON.isNull("Filter")) {
            if (!readerJSON.isNull("KeyConditionExpression"))
                spec = spec.withFilterExpression(readerJSON.getString("FilterExpression"));
            else
                scspec = scspec.withFilterExpression(readerJSON.getString("FilterExpression"));
            JSONArray filterConditions = readerJSON.getJSONArray("Filter");
            filterConditions.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                expressionAttributeValues.put(innerJson.getString("KeyName"), innerJson.get("KeyValue"));
            });

        }
        if (!readerJSON.isNull("ProjectedAttributes")) {
            JSONArray projectedAttributes = readerJSON.getJSONArray("ProjectedAttributes");

            projectedAttributes.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                projectionExpression += "," + innerJson.getString("AttributeName");
                Map<String, Object> inprojectionMap = new HashMap<String, Object>();
                inprojectionMap.put("ColName", innerJson.getString("AttributeName"));
                if (!innerJson.isNull("AliasName")) {
                    //System.out.println("Alias Name -- >" +innerJson.getString("AliasName"));
                    inprojectionMap.put("AliasName", innerJson.getString("AliasName"));
                } else
                    inprojectionMap.put("AliasName", innerJson.getString("AttributeName"));
                projectionMap.put(innerJson.getString("AttributeName"), inprojectionMap);
            });
            projectionExpression = projectionExpression.replaceFirst(",", "");
            //System.out.println(projectionExpression);
            if (!readerJSON.isNull("KeyConditionExpression"))
                spec.withProjectionExpression(projectionExpression);
            else
                scspec.withProjectionExpression(projectionExpression);
        }
        if (!readerJSON.isNull("KeyConditionExpression")) {

            spec = spec.withKeyConditionExpression(readerJSON.getString("KeyConditionExpression"));
            JSONArray keyConditions = readerJSON.getJSONArray("KeyAttributeValues");
            keyConditions.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                expressionAttributeValues.put(innerJson.getString("KeyName"), innerJson.get("KeyValue"));
            });
            spec.withValueMap(expressionAttributeValues);
            ItemCollection<?> items = table.query(spec);
            Iterator<Item> iterator = items.iterator();
            while (iterator.hasNext()) {
                record = iterator.next().toJSONPretty();
                if (!readerJSON.isNull("ProjectedAttributes")) {
                    record = JsonPathReader.readValueFromPath(record, projectionMap, 1);
                    //record=CommonUtils.getInstance().getMappedJSON(record, toJSONSchema);
                }
                returnString += "," + record;
            }
        } else {
            scspec.withValueMap(expressionAttributeValues);
            ItemCollection<?> items = table.scan(scspec);
            Iterator<Item> iterator = items.iterator();
            while (iterator.hasNext()) {
                record = iterator.next().toJSONPretty();
                if (!readerJSON.isNull("ProjectedAttributes")) {
                    record = JsonPathReader.readValueFromPath(record, projectionMap, 1);
                    record = CommonUtils.getInstance().getMappedJSON(record, toJSONSchema);
                }
                returnString += "," + record;
            }
        }
        returnString = returnString.replaceFirst(",", "") + "]";
        //System.out.println(returnString);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(returnString);
        String prettyJsonString = gson.toJson(je);
        // System.out.println(prettyJsonString);
        return prettyJsonString;
    }

    public String readFromDatabase(JSONObject queryExpression) {

        projectionExpression = "";
        Map<String, Object> projectionMap = new HashMap<String, Object>();
        Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
        String returnString = "[";
        String record = "";
        JSONObject readerJSON = queryExpression;
        String tableName = readerJSON.getString("Table");
        Table table = dynamoDB.getTable(tableName);

        QuerySpec spec = new QuerySpec();
        ScanSpec scspec = new ScanSpec();

        if (!readerJSON.isNull("Filter")) {
            if (!readerJSON.isNull("KeyConditionExpression"))
                spec = spec.withFilterExpression(readerJSON.getString("FilterExpression"));
            else
                scspec = scspec.withFilterExpression(readerJSON.getString("FilterExpression"));
            JSONArray filterConditions = readerJSON.getJSONArray("Filter");
            filterConditions.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                expressionAttributeValues.put(innerJson.getString("KeyName"), innerJson.get("KeyValue"));
            });

        }
        if (!readerJSON.isNull("ProjectedAttributes")) {
            JSONArray projectedAttributes = readerJSON.getJSONArray("ProjectedAttributes");

            projectedAttributes.forEach(f -> {
                JSONObject innerJson = (JSONObject) f;
                projectionExpression += "," + innerJson.getString("AttributeName");
                Map<String, Object> inprojectionMap = new HashMap<String, Object>();
                inprojectionMap.put("ColName", innerJson.getString("AttributeName"));
                if (!innerJson.isNull("AliasName")) {
                    //System.out.println("Alias Name -- >" +innerJson.getString("AliasName"));
                    inprojectionMap.put("AliasName", innerJson.getString("AliasName"));
                } else
                    inprojectionMap.put("AliasName", innerJson.getString("AttributeName"));
                projectionMap.put(innerJson.getString("AttributeName"), inprojectionMap);
            });
            projectionExpression = projectionExpression.replaceFirst(",", "");
            //System.out.println(projectionExpression);
            if (!readerJSON.isNull("KeyConditionExpression"))
                spec.withProjectionExpression(projectionExpression);
            else
                scspec.withProjectionExpression(projectionExpression);
        }
        if (!readerJSON.isNull("KeyConditionExpression")) {

            spec = spec.withKeyConditionExpression(readerJSON.getString("KeyConditionExpression"));
            JSONArray keyConditions = readerJSON.getJSONArray("KeyAttributeValues");
            keyConditions.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                expressionAttributeValues.put(innerJson.getString("KeyName"), innerJson.get("KeyValue"));
            });
            spec.withValueMap(expressionAttributeValues);
            ItemCollection<?> items = table.query(spec);
            Iterator<Item> iterator = items.iterator();
            while (iterator.hasNext()) {
                record = iterator.next().toJSONPretty();
                if (!readerJSON.isNull("ProjectedAttributes")) {
                    record = JsonPathReader.readValueFromPath(record, projectionMap, 1);
                    //record=CommonUtils.getInstance().getMappedJSON(record, toJSONSchema);
                }
                returnString += "," + record;
            }
        } else {
            scspec.withValueMap(expressionAttributeValues);
            ItemCollection<?> items = table.scan(scspec);
            Iterator<Item> iterator = items.iterator();
            while (iterator.hasNext()) {
                record = iterator.next().toJSONPretty();
                if (!readerJSON.isNull("ProjectedAttributes")) {
                    record = JsonPathReader.readValueFromPath(record, projectionMap, 1);
                    //record=CommonUtils.getInstance().getMappedJSON(record, toJSONSchema);
                }
                returnString += "," + record;
            }
        }
        returnString = returnString.replaceFirst(",", "") + "]";
        //System.out.println(returnString);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(returnString);
        String prettyJsonString = gson.toJson(je);
        //System.out.println(prettyJsonString);
        return prettyJsonString;
    }

    public String readFromTableIndex(JSONObject queryExpression) {

        projectionExpression = "";
        Map<String, Object> projectionMap = new HashMap<String, Object>();
        Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
        String returnString = "[";
        String record = "";
        JSONObject readerJSON = queryExpression;
        String tableName = readerJSON.getString("Table");
        Table table = dynamoDB.getTable(tableName);
        Index index = table.getIndex(readerJSON.getString("IndexName"));

        QuerySpec spec = new QuerySpec();
        ScanSpec scspec = new ScanSpec();

        if (!readerJSON.isNull("Filter")) {
            if (!readerJSON.isNull("KeyConditionExpression"))
                spec = spec.withFilterExpression(readerJSON.getString("FilterExpression"));
            else
                scspec = scspec.withFilterExpression(readerJSON.getString("FilterExpression"));
            JSONArray filterConditions = readerJSON.getJSONArray("Filter");
            filterConditions.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                expressionAttributeValues.put(innerJson.getString("KeyName"), innerJson.get("KeyValue"));
            });

        }
        if (!readerJSON.isNull("ProjectedAttributes")) {
            JSONArray projectedAttributes = readerJSON.getJSONArray("ProjectedAttributes");

            projectedAttributes.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                projectionExpression += "," + innerJson.getString("AttributeName");
                Map<String, Object> inprojectionMap = new HashMap<String, Object>();
                inprojectionMap.put("ColName", innerJson.getString("AttributeName"));
                if (!innerJson.isNull("AliasName")) {
                    //System.out.println("Alias Name -- >" +innerJson.getString("AliasName"));
                    inprojectionMap.put("AliasName", innerJson.getString("AliasName"));
                } else
                    inprojectionMap.put("AliasName", innerJson.getString("AttributeName"));
                projectionMap.put(innerJson.getString("AttributeName"), inprojectionMap);
            });
            projectionExpression = projectionExpression.replaceFirst(",", "");
            //System.out.println(projectionExpression);
            if (!readerJSON.isNull("KeyConditionExpression"))
                spec.withProjectionExpression(projectionExpression);
            else
                scspec.withProjectionExpression(projectionExpression);
        }
        if (!readerJSON.isNull("KeyConditionExpression")) {

            spec = spec.withKeyConditionExpression(readerJSON.getString("KeyConditionExpression"));
            Object obj = readerJSON.get("KeyAttributeValues");
            if (obj instanceof JSONArray) {
                JSONArray keyConditions = readerJSON.getJSONArray("KeyAttributeValues");
                keyConditions.forEach(f -> {
                    JSONObject innerJson = new JSONObject(f.toString());
                    expressionAttributeValues.put(innerJson.getString("KeyName"), innerJson.get("KeyValue"));
                });
            } else {
                JSONObject innerJson = readerJSON.getJSONObject("KeyAttributeValues");
                expressionAttributeValues.put(innerJson.getString("KeyName"), innerJson.get("KeyValue"));
            }
            spec.withValueMap(expressionAttributeValues);
            ItemCollection<?> items = index.query(spec);
            Iterator<Item> iterator = items.iterator();
            while (iterator.hasNext()) {
                record = iterator.next().toJSONPretty();
                if (!readerJSON.isNull("ProjectedAttributes")) {
                    record = JsonPathReader.readValueFromPath(record, projectionMap, 1);
                    //record=CommonUtils.getInstance().getMappedJSON(record, toJSONSchema);
                }
                returnString += "," + record;
            }
        } else {
            scspec.withValueMap(expressionAttributeValues);
            ItemCollection<?> items = table.scan(scspec);
            Iterator<Item> iterator = items.iterator();
            while (iterator.hasNext()) {
                record = iterator.next().toJSONPretty();
                if (!readerJSON.isNull("ProjectedAttributes")) {
                    record = JsonPathReader.readValueFromPath(record, projectionMap, 1);
                    //record=CommonUtils.getInstance().getMappedJSON(record, toJSONSchema);
                }
                returnString += "," + record;
            }
        }
        returnString = returnString.replaceFirst(",", "") + "]";
        //System.out.println(returnString);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(returnString);
        String prettyJsonString = gson.toJson(je);
        // System.out.println(prettyJsonString);
        return prettyJsonString;
    }

    public String readFirstRecordFromTableIndex(JSONObject queryExpression) {

        projectionExpression = "";
        Map<String, Object> projectionMap = new HashMap<String, Object>();
        Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
        String returnString = "[";
        String record = "";
        JSONObject readerJSON = queryExpression;
        String tableName = readerJSON.getString("Table");
        Table table = dynamoDB.getTable(tableName);
        Index index = table.getIndex(readerJSON.getString("IndexName"));

        QuerySpec spec = new QuerySpec();
        ScanSpec scspec = new ScanSpec();

        if (!readerJSON.isNull("Filter")) {
            if (!readerJSON.isNull("KeyConditionExpression"))
                spec = spec.withFilterExpression(readerJSON.getString("FilterExpression"));
            else
                scspec = scspec.withFilterExpression(readerJSON.getString("FilterExpression"));
            JSONArray filterConditions = readerJSON.getJSONArray("Filter");
            filterConditions.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                expressionAttributeValues.put(innerJson.getString("KeyName"), innerJson.get("KeyValue"));
            });

        }
        if (!readerJSON.isNull("ProjectedAttributes")) {
            JSONArray projectedAttributes = readerJSON.getJSONArray("ProjectedAttributes");

            projectedAttributes.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                projectionExpression += "," + innerJson.getString("AttributeName");
                Map<String, Object> inprojectionMap = new HashMap<String, Object>();
                inprojectionMap.put("ColName", innerJson.getString("AttributeName"));
                if (!innerJson.isNull("AliasName")) {
                    //System.out.println("Alias Name -- >" +innerJson.getString("AliasName"));
                    inprojectionMap.put("AliasName", innerJson.getString("AliasName"));
                } else
                    inprojectionMap.put("AliasName", innerJson.getString("AttributeName"));
                projectionMap.put(innerJson.getString("AttributeName"), inprojectionMap);
            });
            projectionExpression = projectionExpression.replaceFirst(",", "");
            //System.out.println(projectionExpression);
            if (!readerJSON.isNull("KeyConditionExpression"))
                spec.withProjectionExpression(projectionExpression);
            else
                scspec.withProjectionExpression(projectionExpression);
        }
        if (!readerJSON.isNull("KeyConditionExpression")) {

            spec = spec.withKeyConditionExpression(readerJSON.getString("KeyConditionExpression"));
            Object obj = readerJSON.get("KeyAttributeValues");
            if (obj instanceof JSONArray) {
                JSONArray keyConditions = readerJSON.getJSONArray("KeyAttributeValues");
                keyConditions.forEach(f -> {
                    JSONObject innerJson = new JSONObject(f.toString());
                    expressionAttributeValues.put(innerJson.getString("KeyName"), innerJson.get("KeyValue"));
                });
            } else {
                JSONObject innerJson = readerJSON.getJSONObject("KeyAttributeValues");
                expressionAttributeValues.put(innerJson.getString("KeyName"), innerJson.get("KeyValue"));
            }
            spec.withValueMap(expressionAttributeValues);
            spec.setMaxResultSize(1);
            ItemCollection<?> items = index.query(spec);
            Iterator<Item> iterator = items.iterator();
            while (iterator.hasNext()) {
                record = iterator.next().toJSONPretty();
                if (!readerJSON.isNull("ProjectedAttributes")) {
                    record = JsonPathReader.readValueFromPath(record, projectionMap, 1);
                    //record=CommonUtils.getInstance().getMappedJSON(record, toJSONSchema);
                }
                returnString += "," + record;
            }
        } else {
            scspec.withValueMap(expressionAttributeValues);
            ItemCollection<?> items = table.scan(scspec);
            Iterator<Item> iterator = items.iterator();
            while (iterator.hasNext()) {
                record = iterator.next().toJSONPretty();
                if (!readerJSON.isNull("ProjectedAttributes")) {
                    record = JsonPathReader.readValueFromPath(record, projectionMap, 1);
                    //record=CommonUtils.getInstance().getMappedJSON(record, toJSONSchema);
                }
                returnString += "," + record;
            }
        }
        returnString = returnString.replaceFirst(",", "") + "]";
        //System.out.println(returnString);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(returnString);
        String prettyJsonString = gson.toJson(je);
        // System.out.println(prettyJsonString);
        return prettyJsonString;
    }


    public String readFromThingTableIndex(String queryExpression, String projections) {

        Table table = dynamoDB.getTable("project-cage-apps-dev-thing");
        Index index = table.getIndex("name-actorId-index");
        String strResponse = "";

        KeyAttribute kb = new KeyAttribute("name", queryExpression);


        QuerySpec querySpec = new QuerySpec().withHashKey(kb).withProjectionExpression(projections);

        ItemCollection<QueryOutcome> items = null;
        Iterator<Item> iterator = null;
        Item item = null;
        String returnString = "{\"data\":[";
        try {
            items = index.query(querySpec);
            iterator = items.iterator();
            while (iterator.hasNext()) {
                item = iterator.next();
                String record = item.toJSON();
                returnString = returnString += "," + record;
            }
            returnString = returnString.replaceFirst(",", "") + "]}";
        } catch (Exception e) {
            System.err.println("Unable to query ");
            System.err.println(e.getMessage());
        }

        return returnString;

    }

    void getData(String actorId, Long startTimestamp, Long endTimestamp) {
        Table table = dynamoDB.getTable("project-cage-apps-dev-thing");

        QuerySpec spec = new QuerySpec()
                .withKeyConditionExpression("actorId = :v_actorid")
                .withFilterExpression("descriptors.startTime >= :v_startTime and descriptors.startTime <= :v_endTime")
                .withValueMap(new ValueMap()
                        .withString(":v_actorid", actorId)
                        .withLong(":v_startTime", startTimestamp)
                        .withLong(":v_endTime", endTimestamp)
                );

        ItemCollection<QueryOutcome> items = table.query(spec);

        Iterator<Item> iterator = items.iterator();
        Item item = null;
        while (iterator.hasNext()) {
            item = iterator.next();
            System.out.println(item.toJSONPretty());
        }
    }

}
