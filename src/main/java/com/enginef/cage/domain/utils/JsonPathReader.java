package com.enginef.cage.domain.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import com.jayway.jsonpath.JsonPath;

public class JsonPathReader {

    public static Object readValueFromPath(Map<String, Object> data, String path) {
        String preparedPath = preparePath(path);
        Object retData = JsonPath.read(data, preparedPath);
        return retData;
    }

    public static String readValueFromPath(String json, Map<String, Object> paths, int ind) {
        //System.out.println(json);
        HashMap<String, Object> retValue = new HashMap<String, Object>();
        paths.forEach((f, v) ->
        {
            HashMap<String, Object> innerMap = (HashMap<String, Object>) v;
            System.out.println(f.toString());
            String preparedPath = preparePath(innerMap.get("ColName").toString());
            try {
                Object retData = JsonPath.read(new JSONObject(json).toMap(), preparedPath);
                retValue.put(innerMap.get("AliasName").toString(), retData);
            } catch (Exception e) {
            }
        });
        System.out.println("Return Value --->" + retValue);

        return new JSONObject(retValue).toString();
    }

    private static String preparePath(String path) {
        String pathCleared = StringUtils.replaceEach(path, new String[]{"{", "}"}, new String[]{"", ""});
        StringBuilder returnPath = new StringBuilder();
        String[] pathItems = pathCleared.split("\\.");
        for (String pathItem : pathItems) {
            returnPath = returnPath.append(".");
            if (pathItem.indexOf(' ') > -1) {
                returnPath = returnPath.append("['").append(pathItem).append("']");
            } else {
                returnPath = returnPath.append(pathItem);
            }
        }
        return "$" + returnPath.toString();
    }
}
