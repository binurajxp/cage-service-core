package com.enginef.cage.domain.utils;

import java.security.MessageDigest;

import java.util.UUID;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicLong;
import com.datastax.driver.core.utils.UUIDs;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.ReadContext;

public class CommonUtils {
    private static final String UUID_KEY = "UUID";
    private static final String PATH_SEPERATOR = ".";
    private static final String NULL_SCHEMA = "NullSchema";
    private static final String TOP_LEVEL = "TopLevel";
    private static final String X_PATH = "xPath";
    private static final String MAPPED_ATTRIBUTE = "MappedAttribute";
    private static final String ATTRIBUTE_SEPERATOR = "AttributeSeperator";
    private static final String ATTRIBUTE_LIST = "AttributeList";
    private static final String DATA_MAP_FUNCTION = "DataMapFunction";
    private static final String ATTRIBUTE_NAME = "AttributeName";
    private static final String EXPRESSION = "Expression";
    private static final String DATA_MAP = "DataMap";
    private static final String ATTRIBUTE_METADATA = "AttributeMetadata";
    public AtomicLong seq = new AtomicLong();
    private Timestamp currentTimestamp;
    public static CommonUtils commonutil;

    public String getCurrentTimestamp() {
        return new Timestamp(System.currentTimeMillis()).toString();
    }

    public void setCurrentTimestamp(Timestamp currentTimestamp) {
        this.currentTimestamp = currentTimestamp;
    }

    public String getMD5(String key) throws Exception {
        String original = key;
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(original.getBytes());
        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for (byte b : digest) {
            sb.append(String.format("%02x", b & 0xff));
        }

        System.out.println("original:" + original);
        System.out.println("digested(hex):" + sb.toString());
        return sb.toString();

    }

    public static CommonUtils getInstance() {
        if (commonutil == null) {
            commonutil = new CommonUtils();
        }
        return commonutil;
    }

    public String getTransactionId(String accountKey) throws Exception {

        // Long nextVal = seq.incrementAndGet();
        UUID idOne = UUID.randomUUID();
        return getMD5(accountKey + "|" + idOne);
    }

    public boolean validateJSONString(String strJSON) {
        try {
            new JSONObject(strJSON);
        } catch (JSONException ex) {
            try {
                new JSONArray(strJSON);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public Long getUUID() {
        return new Long(System.currentTimeMillis());
    }

    public int ordinalIndexOf(String str, String substr, int n) {
        int pos = str.indexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
        return pos;
    }

    @SuppressWarnings("finally")
    public String getConcatValue(String columnList, String seperatorChar, Map<String, Object> hmp) {
        String strRetValue = "";
        ReadContext readContext = JsonPath.parse(hmp,
                Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS));
        try {
            String[] parts = columnList.split(",");
            StringBuilder parseStr = new StringBuilder();
            for (String part : parts) {

                Object val = readContext.read("$." + part);
                parseStr = parseStr.append(val).append(seperatorChar);
            }
            strRetValue = new StringBuilder(parseStr.reverse().toString().replaceFirst(seperatorChar, "")).reverse()
                    .toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return strRetValue;
        }
    }

    @SuppressWarnings("finally")
    public String getmd5(String columnList, String seperatorChar, Map<String, Object> hmp) {
        String strRetValue = "";
        try {
            String[] parts = columnList.split(",");
            StringBuilder parseStr = new StringBuilder();
            for (String part : parts) {
                parseStr = parseStr.append(hmp.get(part)).append(seperatorChar);
            }
            strRetValue = CommonUtils.getInstance()
                    .getMD5(parseStr.reverse().toString().replaceFirst(seperatorChar, ""));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return strRetValue;
        }
    }
    public static String getRandomNum() {
        return String.valueOf((long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L);
    }

    public String getFromPath(String data,String path){
        ReadContext readContext = JsonPath.parse(data,
                Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS));

        return readContext.read("$."+path);
    }
    public String updateJsonAtPath(String data,String path,Object value){
        DocumentContext docoumentContext = JsonPath.parse(data,
                Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS));
        docoumentContext.set("$."+path, value);
        return docoumentContext.jsonString();
    }
    public String getMappedJSON(String jsonString, String jsonSchema) {
        HashMap<String, Object> innerMap = new HashMap<String, Object>();

        DocumentContext docContext = JsonPath.parse(jsonString,
                Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS));
        String topParentString = "";
        if (jsonSchema.compareToIgnoreCase("NullSchema") == 0)
            topParentString = jsonString;
        else {
            Map<String, Object> hmp = new JSONObject(jsonString).toMap();
            JSONArray jsonArr = new JSONObject(jsonSchema).getJSONArray("AttributeMetadata");
            JSONArray jsonValidSchema = new JSONArray();
            jsonArr.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                System.out.println(innerJson);
                if (innerJson.getString("DataMap").equalsIgnoreCase("Expression")
                        || innerJson.getString("DataMap").equalsIgnoreCase("Const")
                        || docContext.read("$." + innerJson.get("MappedAttribute").toString()) != null)

                    jsonValidSchema.put(innerJson);
            });
            jsonValidSchema.forEach(f -> {
                JSONObject innerJson = new JSONObject(f.toString());
                System.out.println(innerJson);
                String xpathkey = "";
                if (innerJson.isNull("xPath"))
                    xpathkey = "TopLevel";
                else
                    xpathkey = innerJson.getString("xPath");

                HashMap<String, Object> currMap = null;
                if (!innerMap.containsKey(xpathkey))
                    currMap = new HashMap<String, Object>();
                else
                    currMap = (HashMap<String, Object>) innerMap.get(xpathkey);
                if (innerJson.getString("DataMap").equalsIgnoreCase("Const"))
                    currMap.put(innerJson.getString("AttributeName"), innerJson.get("MappedAttribute"));
                if (innerJson.getString("DataMap").equalsIgnoreCase("Value"))
                    // currMap.put(innerJson.getString("AttributeName"),
                    // hmp.get(innerJson.getString("MappedAttribute")));
                    currMap.put(innerJson.getString("AttributeName"),
                            docContext.read("$." + innerJson.getString("MappedAttribute")));
                if (innerJson.getString("DataMap").equalsIgnoreCase("Expression")
                        && innerJson.getString("DataMapFunction").equalsIgnoreCase("md5"))
                    currMap.put(innerJson.getString("AttributeName"),
                            getmd5(innerJson.getString("AttributeList").toString(),
                                    innerJson.getString("AttributeSeperator").toString(), hmp));
                if (innerJson.getString("DataMap").equalsIgnoreCase("Expression")
                        && innerJson.getString("DataMapFunction").equalsIgnoreCase("concat"))
                    currMap.put(innerJson.getString("AttributeName"),
                            getConcatValue(innerJson.getString("AttributeList").toString(),
                                    innerJson.getString("AttributeSeperator").toString(), hmp));
                if (innerJson.getString("DataMap").equalsIgnoreCase("Expression")
                        && innerJson.getString("DataMapFunction").equalsIgnoreCase("UUID"))
                    currMap.put(innerJson.getString("AttributeName"), getUUID());

                innerMap.put(xpathkey, currMap);
            });

            HashMap<String, Object> dummyMap = (HashMap<String, Object>) org.apache.commons.lang3.SerializationUtils
                    .clone(innerMap);
            for (String currkey : dummyMap.keySet()) {
                if (currkey.contains(".")) {
                    String parentKey = "";
                    String[] words = currkey.split("\\.");
                    for (String w : words) {
                        if (parentKey.length() == 0)
                            parentKey = w;
                        else
                            parentKey = parentKey + "." + w;
                        if (innerMap.get(parentKey) == null) {
                            HashMap<String, Object> parentMap = new HashMap<String, Object>();
                            innerMap.put(parentKey, parentMap);
                            System.out.println(currkey + "---->" + parentKey);
                        }

                    }
                }
            }
            if (innerMap.get("TopLevel") == null) {
                HashMap<String, Object> parentMap = new HashMap<String, Object>();
                innerMap.put("TopLevel", parentMap);

            }

            for (String currkey : innerMap.keySet()) {
                String parentKey = "";
                if (currkey.compareToIgnoreCase("TopLevel") != 0) {
                    if (!currkey.contains("."))
                        parentKey = "TopLevel";
                    else {
                        int numOccure = currkey.length() - currkey.replace(".", "").length();
                        parentKey = currkey.substring(0, ordinalIndexOf(currkey + ".", ".", numOccure));
                    }

                    HashMap<String, Object> parentMap = (HashMap<String, Object>) innerMap.get(parentKey);
                    parentMap.put(currkey, innerMap.get(currkey));
                    innerMap.put(parentKey, parentMap);
                }
            }
            HashMap<String, Object> topParentMap = (HashMap<String, Object>) innerMap.get("TopLevel");

            topParentString = new JSONObject(topParentMap).toString();
            for (String currkey : innerMap.keySet()) {
                if (currkey.compareToIgnoreCase("TopLevel") != 0) {
                    String revKey = "";
                    if (currkey.lastIndexOf(".") == -1)
                        revKey = currkey;
                    else
                        revKey = currkey.substring(currkey.lastIndexOf(".") + 1, currkey.length());
                    topParentString = topParentString.replaceFirst("\"" + currkey + "\":", "\"" + revKey + "\":");

                }
            }
        }
        System.out.println(topParentString);
        return topParentString;
    }

    public String mergeJSON(String jsonStringA, String jsonStringB) {
        HashMap<String, Object> jsonMapA = (HashMap<String, Object>) new JSONObject(jsonStringA.toString()).toMap();
        HashMap<String, Object> jsonMapB = (HashMap<String, Object>) new JSONObject(jsonStringB.toString()).toMap();
        jsonMapB.forEach((k, v) -> {
            jsonMapA.put(k, v);
        });
        System.out.println(new JSONObject(jsonMapA));
        return new JSONObject(jsonMapA).toString();
    }

    /*
     * public static void main(String args[]) { String
     * str1="{\r\n \"Name\":\"Manoj\",\r\n \"Address\":{\r\n   \"Hname\":\"MK\",\r\n   \"XY\":\"zz\",\r\n   \"Ab\":\"yu\"\r\n }\r\n}"
     * ; String
     * str2="{\r\n \"SecondName\":\"Kamalasanan\",\r\n \"Qualif\":{\r\n   \"Degree\":\"Msc\",\r\n   \"XY\":\"zz\",\r\n   \"Ab\":\"yu\"\r\n }\r\n}"
     * ;
     * str1="{\r\n  \"Journals\": [\r\n    {\r\n      \"Description\": \"Miscellanious Expense\",\r\n      \"ChartOfAccountCode\": 654,\r\n      \"LineAmount\": 907,\r\n      \"JounalNumber\": 1,\r\n      \"TransactionID\": \"12344234XERT124\"\r\n    },\r\n    {\r\n      \"Description\": \"Miscellanious Expense\",\r\n      \"ChartOfAccountCode\": 375,\r\n      \"LineAmount\": 1620,\r\n      \"JounalNumber\": 2,\r\n      \"TransactionID\": \"12344234XERT124\"\r\n    },\r\n    {\r\n      \"Description\": \"Miscellanious Expense\",\r\n      \"ChartOfAccountCode\": 741,\r\n      \"LineAmount\": 954,\r\n      \"JounalNumber\": 3,\r\n      \"TransactionID\": \"12344234XERT124\"\r\n    },\r\n    {\r\n      \"Description\": \"Miscellanious Expense\",\r\n      \"ChartOfAccountCode\": 861,\r\n      \"LineAmount\": 940,\r\n      \"JounalNumber\": 4,\r\n      \"TransactionID\": \"12344234XERT124\"\r\n    }\r\n  ],\r\n  \"StageName\": \"CKJUGHULL68982136\",\r\n  \"WorkflowIDSTC\": \"CKJUGHULL68982136\",\r\n  \"WorkflowID\": \"CKJUGHULL68982136\", \r\n  \"PartyKey\": 10001,\r\n  \"Amount\": 1323.5,\r\n  \"EventKey\": 13131,\r\n  \"TransactionID\": \"12344234XERT124\"\r\n}"
     * ; //
     * str2="{\r\n \"AttributeMetadata\":[   \r\n   {\"AttributeName\":\"EventDataKey\", \r\n   \"DataMap\":\"Expression\", \r\n   \"DataMapFunction\":\"md5\",\r\n   \"AttributeList\":\"PartyKey,EventKey\",\r\n   \"AttributeSeperator\":\"|\"\r\n   },\r\n   {\"AttributeName\":\"EventTimeStamp\", \r\n   \"DataMap\":\"Expression\", \r\n   \"DataMapFunction\":\"UUID\" \r\n   }, \r\n   {\"AttributeName\":\"WorkflowIDSTC\", \r\n   \"DataMap\":\"Value\", \r\n   \"MappedAttribute\":\"WorkflowIDSTC\",\r\n   \"xPath\":\"hello.hello\"\r\n   } \r\n ] \r\n}"
     * ;
     * str1="{\r\n\"PartyKey\":\"X1002\",\r\n\"PartyTypeKey\":12,\r\n\"FirstName\":\"Manoj\",\r\n\"LastName\":\"Kamalasanan\",\r\n\"Profession\":\"SW Engineer\",\r\n\"DOB\":\"1981-05-07\",\r\n\"CreatedDate\":22323213214,\r\n\"EventKey\":13,\r\n\"EventConfigKey\":1\r\n}"
     * ;
     * str2=" {\"AttributeMetadata\":[{\r\n\t\t\t\t  \"AttributeName\": \"FirstName\",\r\n\t\t\t\t  \"DataMap\": \"Value\",  \r\n\t\t\t\t  \"MappedAttribute\": \"FirstName\"\r\n\t\t\t\t},\r\n\t\t\t\t{\r\n\t\t\t\t  \"AttributeName\": \"LastName\",\r\n\t\t\t\t  \"DataMap\": \"Value\",  \r\n\t\t\t\t  \"MappedAttribute\": \"LastName\"\r\n\t\t\t\t},\r\n\t\t\t\t{\r\n\t\t\t\t  \"AttributeName\": \"Profession\",\r\n\t\t\t\t  \"DataMap\": \"Value\",   \r\n\t\t\t\t  \"MappedAttribute\": \"Profession\"\r\n\t\t\t\t,\r\n   \"xPath\":\"hello.hello\"\r\n }, \r\n\t\t\t\t{\r\n\t\t\t\t  \"AttributeName\": \"DOB\",\r\n\t\t\t\t  \"DataMap\": \"Value\",  \t\t\t  \r\n\t\t\t\t  \"MappedAttribute\": \"DOB\"\r\n\t\t\t\t},\r\n\t\t\t\t{\r\n\t\t\t\t  \"AttributeName\": \"CreatedDate\",\r\n\t\t\t\t  \"DataMap\": \"Value\",  \r\n\t\t\t\t  \"MappedAttribute\": \"CreatedDate\"\r\n\t\t\t\t}\r\n\t\t\t\t]\r\n\t\t\t\t}"
     * ; str2=
     * "{\"AttributeMetadata\":[{\"MappedAttribute\":\"FirstName\",\"AttributeName\":\"FirstName\",\"xPath\":\"hello.hello\",\"DataMap\":\"Value\"},{\"MappedAttribute\":\"LastName\",\"AttributeName\":\"LastName\",\"DataMap\":\"Value\"},{\"MappedAttribute\":\"Profession\",\"AttributeName\":\"Profession\",\"DataMap\":\"Value\"},{\"MappedAttribute\":\"DOB\",\"AttributeName\":\"DOB\",\"DataMap\":\"Value\"},{\"MappedAttribute\":\"CreatedDate\",\"AttributeName\":\"CreatedDate\",\"DataMap\":\"Value\"}]}";
     * System.out.println(str2); String
     * str3=CommonUtils1.getInstance().getMappedJSON(str1, str2); //
     * CommonUtils.getInstance().mergeJSON(str1, str3); }
     */
    public static String getUniqueId() {
        return UUIDs.timeBased().toString();
    }

    public static String getCurrentTime()
    {

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(date);
    }

    public static long getUTCMillisecond()
    {
        try {
            Date date = new Date(System.currentTimeMillis());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.parse(dateFormat.format(date)).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0L;
        }
    }

    public static long getLongFormat(String input)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return dateFormat.parse(input).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0L;
        }
    }
    public static String getUTCDate(long input)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return dateFormat.format(new Date(input))+" UTC";
        } catch (Exception e) {
            e.printStackTrace();
            return "01-01-1900 00:00:00 UTC";
        }
    }
    public static JSONArray sortJSONArray(JSONArray input,String keyName )
    {
        try
        {
            JSONArray sortedJsonArray = new JSONArray();

            List<JSONObject> jsonValues = new ArrayList<JSONObject>();
            for (int i = 0; i < input.length(); i++) {
                jsonValues.add(input.getJSONObject(i));
            }
            Collections.sort( jsonValues, new Comparator<JSONObject>() {
                //You can change "Name" with "ID" if you want to sort by ID

                @Override
                public int compare(JSONObject a, JSONObject b) {
                    String valA = new String();
                    String valB = new String();

                    try {
                        valA = (String) a.get(keyName);
                        valB = (String) b.get(keyName);
                    }
                    catch (JSONException e) {
                        //do something
                    }

                    return valA.compareTo(valB);
                    //if you want to change the sort order, simply use the following:
                    //return -valA.compareTo(valB);
                }
            });

            for (int i = 0; i < input.length(); i++) {
                sortedJsonArray.put(jsonValues.get(i));
            }
            return sortedJsonArray;
        }
        catch(Exception e) {
            return new JSONArray();
        }
    }

}

