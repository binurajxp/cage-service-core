package com.enginef.cage.rest.constants;

public class RestApiUrls {

  private static final String BASE_URL = "/cage-service/v1";
  private static final String THING_URL = BASE_URL + "/thing";
  private static final String ACTOR_URL = BASE_URL + "/actor";
  private static final String EVENT_URL = BASE_URL + "/event";
  public static final String THING_URL_BATCH = THING_URL + "/batch";
  public static final String ACTOR_URL_BATCH = ACTOR_URL + "/batch/{actorName}";
  public static final String EVENT_BATCH_BY_NAME = EVENT_URL + "/batch/{eventName}";
  public static final String THING_STRUCTURE = THING_URL + "/structure";
  public static final String THING_STRUCTURE_BATCH = THING_URL + "/structure/batch";
  public static final String THING_STRUCTURE_WITH_STRUCTURE_KEY = THING_URL + "/structure/{structureKey}";
  public static final String THING_WITH_ACTOR_IDS = THING_URL + "/actor";
  public static final String BATCH_UPDATE_THING_BY_IDS = THING_URL + "/actors/{actorId}/batch";
}
