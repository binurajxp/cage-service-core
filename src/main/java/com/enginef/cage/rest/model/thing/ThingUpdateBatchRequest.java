package com.enginef.cage.rest.model.thing;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.UUID;

public final class ThingUpdateBatchRequest {

  private final UUID actorId;

  private final String structureKey;

  private final String filter;

  private final String filterValue;

  private final String updateKey;

  private final String updateValue;

  private final List<String> ids;

  @JsonCreator
  public ThingUpdateBatchRequest(
      @JsonProperty(value = "actorId") UUID actorId,
      @JsonProperty(value = "structureKey") String structureKey,
      @JsonProperty(value = "filter") String filter,
      @JsonProperty(value = "filterValue") String filterValue,
      @JsonProperty(value = "updateKey") String updateKey,
      @JsonProperty(value = "updateValue") String updateValue,
      @JsonProperty(value = "ids") List<String> ids) {
    this.actorId = actorId;
    this.structureKey = structureKey;
    this.filter = filter;
    this.filterValue = filterValue;
    this.updateKey = updateKey;
    this.updateValue = updateValue;
    this.ids = ids;
  }

  public UUID getActorId() {
    return actorId;
  }

  public String getStructureKey() {
    return structureKey;
  }

  public String getFilter() {
    return filter;
  }

  public String getFilterValue() {
    return filterValue;
  }

  public String getUpdateKey() {
    return updateKey;
  }

  public String getUpdateValue() {
    return updateValue;
  }

  public List<String> getIds() {
    return ids;
  }
}
