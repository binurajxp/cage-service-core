package com.enginef.cage.rest.model.thing;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.UUID;

public final class ThingBatchDeleteRequest {

  private final UUID actorId;
  private final List<UUID> ids;
  private final List<String> structureKeys;

  @JsonCreator
  public ThingBatchDeleteRequest(
      @JsonProperty(value = "actorId") UUID actorId,
      @JsonProperty(value = "ids") List<UUID> ids,
      @JsonProperty(value = "structureKeys") List<String> structureKeys) {
    this.actorId = actorId;
    this.ids = ids;
    this.structureKeys = structureKeys;
  }

  public UUID getActorId() {
    return actorId;
  }

  public List<UUID> getIds() {
    return ids;
  }

  public List<String> getStructureKeys() {
    return structureKeys;
  }
}
