package com.enginef.cage.rest.model.thing;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.UUID;

public final class ThingBatchRequest {

  private final List<UUID> actorIds;

  private final String structureKey;

  private final String filter;

  private final String filterValue;

  @JsonCreator
  public ThingBatchRequest(
      @JsonProperty(value = "actorIds") List<UUID> actorIds,
      @JsonProperty(value = "structureKey") String structureKey,
      @JsonProperty(value = "filter") String filter,
      @JsonProperty(value = "filterValue") String filterValue) {
    this.actorIds = actorIds;
    this.structureKey = structureKey;
    this.filter = filter;
    this.filterValue = filterValue;
  }

  public List<UUID> getActorIds() {
    return actorIds;
  }

  public String getStructureKey() {
    return structureKey;
  }

  public String getFilter() {
    return filter;
  }

  public String getFilterValue() {
    return filterValue;
  }
}
