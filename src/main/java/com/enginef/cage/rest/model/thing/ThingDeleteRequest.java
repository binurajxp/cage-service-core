package com.enginef.cage.rest.model.thing;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;

public final class ThingDeleteRequest {

  private final UUID actorId;
  private final UUID id;
  private final String structureKey;
  private final String filter;
  private final String filterValue;

  @JsonCreator
  public ThingDeleteRequest(
      @JsonProperty(value = "actorId") UUID actorId,
      @JsonProperty(value = "id") UUID id,
      @JsonProperty(value = "structureKey") String structureKey,
      @JsonProperty(value = "filter") String filter,
      @JsonProperty(value = "filterValue")String filterValue) {
    this.actorId = actorId;
    this.id = id;
    this.structureKey = structureKey;
    this.filter = filter;
    this.filterValue = filterValue;
  }

  public UUID getActorId() {
    return actorId;
  }

  public UUID getId() {
    return id;
  }

  public String getStructureKey() {
    return structureKey;
  }

  public String getFilter() {
    return filter;
  }

  public String getFilterValue() {
    return filterValue;
  }
}
